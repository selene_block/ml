from neuron import Neuron
from layer import Layer

class NN:
    Layers = []
    def __init__(self, inputs, *layers):
        self.inputs = inputs
        self.create_layers(layers)
    
    def create_layers(self, layers):
        for layer_idx in range(0, len(layers)):
            layer_neurons = layers[layer_idx]

            if layer_idx == 0:
                self.Layers.append(Layer(layer_idx, layer_neurons, self.inputs, layer_mode='input'))
            elif layer_idx == len(layers):
                self.Layers.append(Layer(layer_idx, layer_neurons, layer_mode='output'))
            else:
                self.Layers.append(Layer(layer_idx, layer_neurons))

    def forward(self):
        outputs = self.inputs
        for layer in self.Layers:
            layer.set_inputs(outputs)
            outputs = layer.propagate()

        return outputs
