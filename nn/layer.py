from neuron import Neuron
from typing import List
import numpy as np

class Layer:
    Neurons = []

    def __init__(self, idx: int, neurons: int, inputs: List[float] = [], weights: List[float] = [], layer_mode = 'fully_connected'):
        self.unique_id = idx
        self.weights = weights
        self.inputs = inputs
        self.layer_mode = layer_mode

        self.Neurons = [Neuron(inputs=inputs) for _ in range(0, neurons)]

    def set_inputs(self, inputs):
        print('id', self.unique_id, 'inputs', inputs)
        for neuron in self.Neurons:
            neuron.set_inputs(inputs)
            neuron.set_weight([1, 1])

    def propagate(self):
        outputs = []
        for neuron in self.Neurons:
            outputs.append(neuron.propagate())

        print('id', self.unique_id, 'outputs', outputs)
        return outputs
