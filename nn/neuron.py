import numpy as np
from typing import List

class Neuron:
    def __init__(self, inputs: List[float] = [], bias: float = -1):
        self.inputs = inputs
        self.set_weights([1, 1])
        self.bias = bias

    @staticmethod
    def relu(x):
        return np.maximum(x, 0)

    def set_inputs(self, inputs):
        self.inputs = inputs

    def set_weights(self, weights):
        self.weights = weights

    def propagate(self):
        return Neuron.relu(np.dot(self.inputs, self.weights) + self.bias)

